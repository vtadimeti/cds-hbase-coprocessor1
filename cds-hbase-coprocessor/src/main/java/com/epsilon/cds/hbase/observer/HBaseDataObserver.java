package com.epsilon.cds.hbase.observer;
/**
 * 1. Checks the Empty Value for each KeyValue of a Column
 * 2. Deletes that KeyValue using PUT
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.KeyValue.Type;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.coprocessor.BaseRegionObserver;
import org.apache.hadoop.hbase.coprocessor.ObserverContext;
import org.apache.hadoop.hbase.coprocessor.RegionCoprocessorEnvironment;
import org.apache.hadoop.hbase.regionserver.wal.WALEdit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;


public class HBaseDataObserver extends BaseRegionObserver {
	private static final Logger logger = Logger.getLogger(HBaseDataObserver.class);


	@Override
	public void prePut(ObserverContext<RegionCoprocessorEnvironment> e,
			Put put, WALEdit edit, boolean writeToWAL) throws IOException {
		// TODO Auto-generated method stub
		super.prePut(e, put, edit, writeToWAL);
		//Deleting the Column using PUT during iteration throws, Concurrent ModificationException
		//For all Empty Valued Columns
		List<KeyValue> itemsToDelete = new ArrayList<>();

		byte[] rowKey = put.getRow();
		Map<byte[], List<KeyValue>> familyMap = put.getFamilyMap();
		Iterator<byte[]> itr =  familyMap.keySet().iterator(); 
		byte[] tableName = e.getEnvironment(
				).getRegion().getRegionInfo().getTableName();
		//Iterate familyMap for each column
		while(itr.hasNext()){
			byte[] family = (byte[])itr.next();
			//Get list of KeyValue for a Column
			List<KeyValue> familyMapValues = familyMap.get(family);
			//Iterate KeyValue for a Column
			for (Iterator<KeyValue> iterator = familyMapValues.iterator(); iterator
					.hasNext();) {
				KeyValue keyValue = (KeyValue) iterator.next();
				//Cell Key
				byte[] byteCellKey = keyValue.getKey();
				//Cell Value
				byte[] byteCellValue = keyValue.getValue();
				String value = Bytes.toString(byteCellValue);
				logger.info("CellKeys "+Bytes.toString(byteCellKey)+" CellValue "+value+" Value Size "+value.length());
				logger.info("Family "+Bytes.toString(keyValue.getFamily())+" Qualifier "+Bytes.toString(keyValue.getQualifier())+" TableName "+Bytes.toString(tableName)+" RowKey "+Bytes.toString(rowKey)+" Row1  "+Bytes.toString(family));
				if(value == null || value.length() == 0 || value.equalsIgnoreCase("null")){
					logger.info("Inside value zero "+value);
					//Put Key Value in the list
					itemsToDelete.add(keyValue);
				}
			}
		}
		logger.info("Size of items to delete is "+itemsToDelete.size());
		for(KeyValue kv: itemsToDelete) {
			logger.info("Deleting RowKey: "+kv.getRow()+" Column family: "+ kv.getFamily()+" Qualifer: "+ kv.getQualifier());
			put.add(new KeyValue(kv.getRow(), kv.getFamily(), kv.getQualifier(), HConstants.LATEST_TIMESTAMP, Type.DeleteColumn));
		}
	}
}
